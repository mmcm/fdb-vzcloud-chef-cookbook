
class Chef
  class Knife
    class FdbVerizonClusterDelete < Knife

      banner "knife fdb verizon cluster delete NAME"

      deps do
        require 'chef/data_bag_item'
        require 'chef/search/query'
        require 'chef/api_client'

        require 'verizon_vm_delete'
        VerizonVmDelete.load_deps
        require 'pfsense_dhcp_static_delete'
        PfsenseDhcpStaticDelete.load_deps
      end

      def run
        @cluster_name = @name_args[0]
        if @cluster_name.nil?
          show_usage
          ui.fatal "You must specify a cluster name"
          exit 1
        end

        cluster_item = Chef::DataBagItem.load('fdb_cluster', @cluster_name)

        nodes = []
        node_query = Chef::Search::Query.new
        node_query.search(:node, "fdb_cluster:#{@cluster_name}") do |node|
          nodes << node
        end

        confirm "Do you really want to delete #{@cluster_name} including VMs: #{nodes.map(&:name).join(', ')}"

        nodes.each_with_index do |node, i|
          last_node = i == nodes.length - 1

          vvd = VerizonVmDelete.new
          vvd.merge_configs
          vvd.name_args = [ node.name ]
          vvd.config[:delete_all_disks] = true
          vvd.config[:yes] = true
          vvd.run

          pfd = PfsenseDhcpStaticDelete.new
          pfd.merge_configs
          pfd.name_args = [ node.name ]
          pfd.config[:apply_changes] = last_node
          pfd.config[:yes] = true
          pfd.run

          client = Chef::ApiClient.load(node.name)
          ui.msg "Deleting #{client}"
          client.destroy
          ui.msg "Deleting #{node}"
          node.destroy
        end

        ui.msg "Deleting #{cluster_item}"
        cluster_item.destroy 'fdb_cluster', @cluster_name
      end
    end
  end
end
