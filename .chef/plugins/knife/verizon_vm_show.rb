
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonVmShow < Knife

      include Knife::VerizonBase

      banner "knife verizon vm show NAME"

      def run
        @vm_name = @name_args[0]
        if @vm_name.nil?
          show_usage
          ui.fatal "You must specify a virtual machine name"
          exit 1
        end

        vm, vnics, vdisks = nil, nil, nil
        vz_rest.start do |http|
          vms = vz_rest.get "#{API_ROOT}/vm/?limit=200", http
          vm = vms['items'].detect {|vm| vm['name'] == @vm_name}
          if vm
            vnics = vz_rest.get vm['vnics']['href'], http
            vdisks = vz_rest.get vm['vdiskMounts']['href'], http
            vdisks['items'].each do |vdisk|
              vdisk['vdisk'] = vz_rest.get vdisk['vdisk']['href'], http
            end
          end
        end
        if vm
          ui.output <<-SUMMARY
#{ui.color('Name:', :bold)}        #{ui.color(@vm_name, :bold)}
#{key('Status:')}      #{vm['status']}
#{key('Description:')} #{vm['description']}
#{key('OS:')}          #{vm['os']} (#{vm['arch']})
#{key('Cores:')}       #{vm['processorCores']}
#{key('Memory:')}      #{vm['memory'] / 1024} GB
SUMMARY

          list = ['Network', 'Status', 'MAC', 'Bandwidth'].collect {|h| ui.color(h, :bold)}
          vnics['items'].each do |vnic|
            list << vnic['vnet']['name']
            list << vnic['status']
            list << vnic['mac']
            list << "#{vnic['bandwidth']} MB"
          end
          ui.output ui.list(list, :columns_across, 4)

          list = ['Disk', 'Status', 'Description', 'Size', 'IOPS'].collect {|h| ui.color(h, :bold)}
          vdisks['items'].each do |vdisk|
            list << vdisk['vdisk']['name']
            list << vdisk['status']
            list << vdisk['vdisk']['description']
            list << "#{vdisk['vdisk']['size'] / 1024} GB"
            list << vdisk['diskOps'].to_s
          end
          ui.output ui.list(list, :columns_across, 5)
        end
      end

      def key(key_text)
        ui.color(key_text, :cyan)
      end
    end
  end
end
