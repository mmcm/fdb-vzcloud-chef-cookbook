
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonVmDelete < Knife

      include Knife::VerizonBase

      banner "knife verizon vm delete NAME"

      option :delete_all_disks,
        :long => '--delete-all-disks',
        :description => 'Delete non-boot disks too',
        :boolean => true,
        :default => false

      def run
        @vm_name = @name_args[0]
        if @vm_name.nil?
          show_usage
          ui.fatal "You must specify a virtual machine name"
          exit 1
        end

        confirm "Do you really want to delete #{@vm_name}"

        vz_rest.start do |http|
          vms = vz_rest.get "#{API_ROOT}/vm/?limit=200", http
          vm = vms['items'].detect {|vm| vm['name'] == @vm_name}
          if vm
            if config[:delete_all_disks]
              vdisks = vz_rest.get vm['vdiskMounts']['href'], http
              vdisks['items'].reject! {|vdisk| vdisk['boot']}
              vdisks['items'].each do |vdisk|
                vdisk['vdisk'] = vz_rest.get vdisk['vdisk']['href'], http
              end
            end
            ui.msg "Deleting VM #{@vm_name}"
            vm = vz_rest.delete vm['href'], http
            if config[:delete_all_disks] && !vdisks['items'].empty?
              ui.msg "Waiting for VM to terminate to delete disks"
              vz_rest.wait_for_job vm['href'], http
              if true
                ui.msg "Waiting 60 more seconds"
                sleep 60
              end
              vdisks['items'].each do |vdisk|
                ui.msg "Deleting disk #{vdisk['vdisk']['name']}"
                vz_rest.delete vdisk['vdisk']['href'], http
              end
            end
          else
            ui.fatal "Virtual machine not found"
            exit 1
          end
        end
      end
    end
  end
end
