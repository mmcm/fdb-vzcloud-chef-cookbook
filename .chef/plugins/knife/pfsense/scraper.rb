
require 'net/http'

module Pfsense

  class Scraper
    def initialize(host, port, user, password)
      @host, @port, @user, @password = host, port, user, password
    end

    HOME_PATH = '/index.php'
    FIND_MAGIC = %r|<input type='hidden' name='__csrf_magic' value=\"(.+)\" />|
    DHCP_PATH = '/services_dhcp.php'
    DHCP_EDIT_PATH = '/services_dhcp_edit.php'
    IFACE = 'lan'
    FIND_CELL = %r|ondblclick=\"document.location='services_dhcp_edit.php\?if=#{IFACE}&id=(\d+?)';\">(.*?)</td>|m
    FIND_RANGE_FROM = %r|<input name=\"range_from\".* id="range_from".* value="(.+)">|
    FIND_RANGE_TO = %r|<input name=\"range_to\".* id="range_to".* value="(.+)">|

    def session
      http = Net::HTTP.new(@host, @port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      http.start do |http|
        
        response = http.get(HOME_PATH)
        session = {
          :cookies => response.get_fields('set-cookie').map {|c| c.split('; ')[0]}.join('; '),
          :csrf_magic => response.body =~ FIND_MAGIC && $1
        }
        data = form_data(session,
                         'login' => 'Login',
                         'usernamefld' => @user,
                         'passwordfld' => @password)
        response = http.post(HOME_PATH, data, headers(session, true))
        
        result = yield(session, http)

        response = http.get("#{HOME_PATH}?logout", headers(session))

        result
      end
    end

    def headers(session, content = false)
      headers = { 'Cookie' => session[:cookies] }
      headers['Content-Type'] = 'application/x-www-form-urlencoded' if content
      headers
    end

    def form_data(session, fields)
        fields['__csrf_magic'] = session[:csrf_magic]
        URI.encode_www_form(fields)
    end
    
    def service_dhcp_static(session, http)
      mappings = []
      response = http.get(DHCP_PATH, headers(session))
      body = response.body
      pos = 0
      while m = FIND_CELL.match(body, pos)
        idx = m[1].to_i
        (mappings[idx] ||= []) << trim_html(m[2])
        pos = m.end(0)
      end
      mappings
    end

    def service_dhcp_static_add(session, http, options)
      data = form_data(session, options.merge('if' => IFACE, 'Submit' => 'Save'))
      response = http.post(DHCP_EDIT_PATH, data, headers(session, true))
    end

    def service_dhcp_static_del(session, http, id)
      query = URI.encode_www_form('if' => IFACE, 'id' => id, 'act' => 'del')
      response = http.get("#{DHCP_PATH}?#{query}", headers(session))
    end

    def service_dhcp_apply(session, http)
      response = http.get(DHCP_PATH, headers(session))
      body = response.body
      range_from = body =~ FIND_RANGE_FROM && $1
      range_to = body =~ FIND_RANGE_TO && $1
      raise "Did not find existing DHCP range" unless range_from && range_to
      data = form_data(session,
                       'if' => IFACE, 
                       'apply' => 'Apply changes', 
                       'enable' => 'yes',
                       'range_from' => range_from,
                       'range_to' => range_to)
      response = http.post(DHCP_PATH, data, headers(session, true))
    end

    def trim_html(str)
      str.gsub(/&nbsp;/,' ').strip
    end
  end
end
