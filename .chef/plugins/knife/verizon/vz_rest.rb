
require 'base64'
require 'json'
require 'net/http'
require 'openssl'
require 'cgi'

module Net
  class HTTP
    class OptionsEx < HTTPRequest
      METHOD = 'OPTIONS'
      REQUEST_HAS_BODY = false
      RESPONSE_HAS_BODY = true  # Need this for API below
    end
  end
end

module Verizon

  class VzRest
    attr_accessor :logger

    def initialize(host, port, access_key, secret_key, logger = nil)
      @host, @port, @access_key, @secret_key, @logger = host, port, access_key, secret_key, logger
    end

    def new_http
      Net::HTTP.new(@host, @port).tap do |http|
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
    end

    def start(http = new_http, &block)
      logger.debug "CONNECT #{@host}" if logger
      http.start &block
    end
    
    def get(uri, http = new_http)
      req = Net::HTTP::Get.new(uri_path(uri))
      add_type_headers_and_sign req, http
      json_request(req, nil, http)
    end

    def post(uri, data, http = new_http)
      data = JSON.dump(data) if data.is_a?(Hash)
      req = Net::HTTP::Post.new(uri_path(uri))
      add_type_headers_and_sign req, http
      json_request(req, data, http)
    end

    def delete(uri, http = new_http)
      req = Net::HTTP::Delete.new(uri_path(uri))
      add_type_headers_and_sign req, http
      json_request(req, nil, http)
    end

    def wait_for_job(uri, http = new_http)
      begin
        sleep 1
        # Doc says this will become a HTTPSeeOther at some point, but
        # that does not seem to happen.
        job = get(uri, http)
      end until job['status'] != 'IN_PROGRESS'
      raise job['errorMessage'] if job['status'] == 'FAILED' # No idea.
    end

    def uri_path(uri)
      case uri
      when /^https:/ then URI(uri).path
      else uri end
    end

    def json_request(request, body, http)
      logger.debug "#{request.method} #{request.path}" if logger
      logger.debug body.inspect if body && logger
      response = http.request(request, body)
      json = response.body && JSON.parse(response.body)
      logger.debug json.inspect if json && logger
      case response
      when Net::HTTPSuccess then json
      else
        raise json ? json['message'] : "Failed: #{response}"
      end
    end

    def sign(request)
      request['Date'] ||= Time.now.utc.strftime('%a, %d %b %Y %H:%M:%S GMT')
      uri = URI(request.path)
      base_path = 
      lines = [
        request.method,
        request['Content-type'] || '',
        request['Date'],
        '',                     # Canonicalized headers (x-tmrk-xxx).
        uri.path
      ]
      if uri.query
        params = CGI::parse(uri.query).to_a
        params.sort! {|p1,p2| p1.first <=> p2.first}
        params.each do |p,v|
          lines << "#{p}:#{v[0]}"
        end
      end
      lines << nil              # Extra newline at end.
      digest = OpenSSL::Digest::Digest.new("sha256")
      signature = Base64.strict_encode64(OpenSSL::HMAC.digest(digest,
                                                              @secret_key,
                                                              lines.join("\n")))
      request['x-tmrk-authorization'] = "CloudApi AccessKey=#{@access_key} SignatureType=HmacSHA256 Signature=#{signature}"
      request
    end

    def add_type_headers_and_sign(request, http = new_http)
      base_path = URI(request.path).path # Strip any query
      options_request = Net::HTTP::OptionsEx.new(base_path)
      sign options_request
      options = json_request(options_request, nil, http)
      types = options['methods'][request.method]
      request['Content-type'] = types['requestType'] if types['requestType']
      request['Accept'] = types['responseType'] if types['responseType']
      sign request
    end
    
  end
end
