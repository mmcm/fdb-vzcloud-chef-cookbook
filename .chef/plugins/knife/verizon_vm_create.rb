
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonVmCreate < Knife

      include Knife::VerizonBase

      banner "knife verizon vm create NAME VM_TEMPLATE DISK_TEMPLATE (options)"

      option :description,
        :short => '-d',
        :long => '--description DESC',
        :description => 'Description of the new VM'

      option :disk_iops,
        :long => '--disk-iops IOPS',
        :description => 'Disk performance',
        :default => 100
      
      option :data_disks,
        :long => '--data-disks COUNT',
        :description => 'Number of data volumes to mount',
        :default => 0
      
      option :data_disk_size,
        :long => '--data-disk-size GB',
        :description => 'Size of each data disk',
        :default => 10

      option :data_disk_iops,
        :long => '--data-disk-iops IOPS',
        :description => 'Data disk performance'

      option :network,
        :short => '-n',
        :long => '--network NET',
        :description => 'Name of attached network'

      option :network_bandwidth,
        :long => '--network-bandwidth MBS',
        :description => 'Bandwidth for virtual network',
        :default => 200

      option :network_interfaces,
        :long => '--network-interfaces COUNT',
        :description => 'Number of NIC to assign',
        :default => 1

      def run
        if @name_args.length < 3
          show_usage
          ui.fatal "You must specify a name and vm and disk templates"
          exit 1
        end
        name, vm_template_name, vdisk_template_name = @name_args
        
        vm = {
          'name' => name
        }
        vm['description'] = config[:description] if config[:description]

        vz_rest.start do |http|
          vm_templates = vz_rest.get "#{API_ROOT}/vm-template/", http
          vm_template = vm_templates['items'].detect {|temp| temp['name'] == vm_template_name}
          if vm_template
            # Doc says fromVirtualMachineTemplate in one place
            vm['fromVmTemplate'] = vm_template.select {|key| ['href','type'].include?(key)}
          else
            ui.fatal "vm template not found"
            exit 1
          end

          vdisk_templates = vz_rest.get "#{API_ROOT}/vdisk-template/", http
          vdisk_template = vdisk_templates['items'].detect {|temp| temp['name'] == vdisk_template_name}
          if vdisk_template
            vm['vdiskMounts'] = {
              'items' => [{
                  'index' => 0,
                  'vdisk' => {
                    'name' => "#{name}: Boot Disk",
                    'description' => "Boot disk for #{name}",
                    'fromVdiskTemplate' => vdisk_template.select {|key| ['href','type'].include?(key)}
                  },
                  'diskOps' => config[:disk_iops]
                }]
            }
          else
            ui.fatal "disk template not found"
            exit 1
          end

          config[:data_disks].to_i.times do |n|
            index = n + 1
            vdisk = vz_rest.post "#{API_ROOT}/vdisk", {
              'name' => "#{name}: Data Disk #{index}",
              'description' => "Data disk #{index} for #{name}",
              'size' =>  config[:data_disk_size] * 1024
            }, http
            vm['vdiskMounts']['items'] << {
              'index' => index,
              'vdisk' => vdisk['target'].select {|key| ['href','type'].include?(key)},
              'diskOps' => config[:data_disk_iops] || config[:disk_iops]
            }
          end

          if config[:network]
            vnets = vz_rest.get "#{API_ROOT}/vnet/", http
            vnet = vnets['items'].detect {|vnet| vnet['name'] == config[:network]}
            if vnet
              vnet = vnet.select {|key| ['href','type'].include?(key)}
              vm['vnics'] = {
                'items' => (1..config[:network_interfaces].to_i).collect do |n|
                  { 'number' => n, 'vnet' => vnet,
                    'bandwidth' => config[:network_bandwidth] 
                  }
                end
              }
            else
              ui.fatal "network not found"
              exit 1
            end
          end

          vm = vz_rest.post "#{API_ROOT}/vm/", vm, http
        end
      end
    end
  end
end
