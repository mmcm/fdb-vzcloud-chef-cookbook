
class Chef
  class Knife
    class FdbClusterList < Knife

      banner "knife fdb cluster list"

      deps do
        require 'chef/data_bag'
      end

      def run
        if data_bag = Chef::DataBag.load('fdb_cluster')
          puts ui.list(data_bag.collect {|id,url| id})
        end
      end
    end
  end
end
