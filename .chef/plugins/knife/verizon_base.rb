
class Chef
  class Knife
    module VerizonBase

      API_ROOT = '/api/compute'

      def self.included(base)
        base.class_eval do

          deps do
            require 'verizon/vz_rest'
          end

          option :access_key,
            :short => '-a',
            :long => '--access-key KEY',
            :description => 'API access key',
            :proc => Proc.new {|key| Chef::Config[:knife][:vzcloud_api_access_key] = key}

          option :secret_key,
            :short => '-s',
            :long => '--secret-key KEY',
            :description => 'API secret key',
            :proc => Proc.new {|key| Chef::Config[:knife][:vzcloud_api_secret_key] = key}

          option :host,
            :short => '-h',
            :long => '--host HOST',
            :description => 'Cloud API host',
            :default => 'iadg2.cloud.verizon.com'

          option :port,
            :short => '-p',
            :long => '--port PORT',
            :description => 'Cloud API port',
            :default => 443

        end
      end

      def vz_rest
        @vz_rest ||= begin
          access_key = Chef::Config[:knife][:vzcloud_api_access_key]
          secret_key = Chef::Config[:knife][:vzcloud_api_secret_key]

          unless access_key && secret_key
            show_usage
            ui.fatal "You must specify access credentials here or in knife.rb"
            exit 1
          end

          Verizon::VzRest.new(config[:host], config[:port], access_key, secret_key,
                              Chef::Log.level == :debug && Chef::Log.logger)
        end
      end
    end
  end
end
