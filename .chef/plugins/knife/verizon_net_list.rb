
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonNetList < Knife

      include Knife::VerizonBase

      banner "knife verizon net list"

      def run
        vnets = nil
        vz_rest.start do |http|
          vnets = vz_rest.get "#{API_ROOT}/vnet/", http
        end
        list = ['Name', 'Description'].collect {|h| ui.color(h, :bold)}
        vnets['items'].each do |vnet|
          ['name', 'description'].each do |key|
            list << vnet[key]
          end
        end
        puts ui.list(list, :columns_across, 2)
      end
    end
  end
end
