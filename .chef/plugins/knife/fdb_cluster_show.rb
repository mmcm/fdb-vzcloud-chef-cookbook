
class Chef
  class Knife
    class FdbClusterShow < Knife

      banner "knife fdb cluster show NAME"

      deps do
        require 'chef/data_bag_item'
        require 'chef/search/query'
      end

      def run
        @cluster_name = @name_args[0]
        if @cluster_name.nil?
          show_usage
          ui.fatal "You must specify a cluster name"
          exit 1
        end

        cluster_item = Chef::DataBagItem.load('fdb_cluster', @cluster_name)
        coordinators = []
        servers = []
        clients = []
        node_query = Chef::Search::Query.new
        node_query.search(:node, "fdb_cluster:#{@cluster_name}") do |node|
          if node['fdb']['server']
            node['fdb']['server'].each do |serv|
              addr = "#{node.name}:#{serv['id']}"
              if serv['coordinator']
                coordinators << addr
              else
                servers << addr
              end
            end
          else
            clients << node.name
          end
        end

        ui.output <<-SUMMARY
#{ui.color('Cluster Name:', :bold)} #{ui.color(@cluster_name, :bold)}
#{key('Redundancy:')}   #{cluster_item['redundancy']}
#{key('Storage:')}      #{cluster_item['storage']}
#{key('Coordinators:')} #{coordinators.sort.join(', ')}
#{key('Servers:')}      #{servers.sort.join(', ')}
#{key('Clients:')}      #{clients.sort.join(', ')}
SUMMARY
      end

      def key(key_text)
        ui.color(key_text, :cyan)
      end
    end
  end
end
