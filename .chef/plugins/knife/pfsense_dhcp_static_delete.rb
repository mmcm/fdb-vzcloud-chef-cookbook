
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'pfsense_base'

class Chef
  class Knife
    class PfsenseDhcpStaticDelete < Knife

      include Knife::PfsenseBase

      banner "knife pfsense dhcp static delete NAME/ADDR"

      option :apply_changes,
        :long => '--apply-changes',
        :description => 'Apply changes to running server',
        :boolean => true,
        :default => true

      def run
        @name_arg = @name_args[0]
        if @name_arg.nil?
          show_usage
          ui.fatal "You must specify a cluster name"
          exit 1
        end

        confirm "Do you really want to delete mapping for #{@name_arg}"

        pfsense.session do |session, http|
          mapping, id = nil, nil
          pfsense.service_dhcp_static(session, http).each_with_index do |row, i|
            if row.include?(@name_arg)
              mapping, id = row, i
              break
            end
          end
          if id
            ui.msg "Deleting static map for #{mapping[1]} to #{mapping[2]}"
            pfsense.service_dhcp_static_del session, http, id
            pfsense.service_dhcp_apply session, http if config[:apply_changes]
          else
            ui.fatal "Static mapping not found: #{@name_arg}"
            exit 1
          end
        end
      end

    end
  end
end
