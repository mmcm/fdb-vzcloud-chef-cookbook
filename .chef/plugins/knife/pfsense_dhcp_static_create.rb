
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'pfsense_base'

class Chef
  class Knife
    class PfsenseDhcpStaticCreate < Knife

      include Knife::PfsenseBase

      banner "knife pfsense dhcp static create MAC IP [NAME [DESC]]"

      option :apply_changes,
        :long => '--apply-changes',
        :description => 'Apply changes to running server',
        :boolean => true,
        :default => true

      def run
        if @name_args.length < 2
          show_usage
          ui.fatal "You must specify a MAC address and IP address"
          exit 1
        end
        mac, ipaddr, hostname, descr = @name_args

        pfsense.session do |session, http|
          pfsense.service_dhcp_static_add session, http,
            'mac' => mac, 'ipaddr' => ipaddr,
            'hostname' => hostname || '', 'descr' => descr || ''
          pfsense.service_dhcp_apply session, http if config[:apply_changes]
        end
      end

    end
  end
end
