
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonVmTemplateList < Knife

      include Knife::VerizonBase

      banner "knife verizon vm template list"

      def run
        templates = nil
        vz_rest.start do |http|
          templates = vz_rest.get "#{API_ROOT}/vm-template/", http
        end
        list = ['Name', 'Description'].collect {|h| ui.color(h, :bold)}
        templates['items'].each do |temp|
          ['name', 'description'].each do |key|
            list << temp[key]
          end
        end
        puts ui.list(list, :columns_across, 2)
      end
    end
  end
end
