
class Chef
  class Knife
    class FdbVerizonClusterCreate < Knife

      banner "knife fdb verizon cluster create NAME NUM_FDB NUM_SQL (options)"

      option :vdisk_template,
        :long => '--vdisk-template TEMPLATE',
        :description => 'Disk template to use for creating VMs',
        :default => 'ubuntu-12.04.4-server-amd64'

      option :fdb_vm_template,
        :long => '--fdb-vm-template TEMPLATE',
        :description => 'VM template to use for creating VM for FDB',
        :default => 'Large-4V-8GB'

      option :sql_vm_template,
        :long => '--sql-vm-template TEMPLATE',
        :description => 'VM template to use for creating VM for SQL',
        :default => 'Extra-Large-Memory-8V-28GB'

      option :start_ip_address,
        :long => '--start-ip-address IPADDR',
        :description => 'Starting IP address'

      option :fdb_processes,
        :long => '--fdb-processes NUM',
        :description => 'Number of processes / data disks for FDB',
        :default => 2

      option :run_chef_client,
        :long => '--[no-]chef-client',
        :description => 'Run chef client on the cluster nodes at the end',
        :boolean => true,
        :default => true

      deps do
        require 'fdb_cluster_create'
        FdbClusterCreate.load_deps
        require 'verizon_vm_create'
        VerizonVmCreate.load_deps
        require 'pfsense_dhcp_static_create'
        PfsenseDhcpStaticCreate.load_deps
        require 'chef/knife/ssh'
        Chef::Knife::Ssh.load_deps
        require 'chef/knife/bootstrap'
        Chef::Knife::Bootstrap.load_deps
        require 'chef/node'
        require 'ipaddr'
      end

      def run
        if @name_args.length < 3
          show_usage
          ui.fatal "You must specify cluster name and number of servers"
          exit 1
        end
        @cluster, num_fdb, num_sql = @name_args
        num_fdb = num_fdb.to_i
        num_sql = num_sql.to_i

        @vm_create = VerizonVmCreate.new
        @vm_create.merge_configs

        @dhcp_create = PfsenseDhcpStaticCreate.new
        @dhcp_create.merge_configs

        @next_ip_address = config[:start_ip_address]
        if @next_ip_address.nil?
          max_addr = nil
          pfsense = @dhcp_create.pfsense
          pfsense.session do |session, http|
            pfsense.service_dhcp_static(session, http).each do |row|
              addr = IPAddr.new(row[2]).to_i
              max_addr = addr if max_addr.nil? || max_addr < addr
            end
          end
          @next_ip_address = IPAddr.new(max_addr + 1, Socket::AF_INET).to_s
        end

        @nodes = []
        fdb_name_index, sql_name_index = 0, 0
        @new_cluster = true
        
        # Is there already a cluster by this name?
        if data_bag = Chef::DataBag.load('fdb_cluster')
          if data_bag[@cluster]
            node_query = Chef::Search::Query.new
            node_query.search(:node, "fdb_cluster:#{@cluster}") do |node|
              @nodes << {
                :name => node.name,
                :node => node,
                :ipaddress => node['ipaddress'],
                :fdb => node['fdb'],
                :iptables => node['iptables']
              }
            end
            @nodes.each do |node|
              case node[:name]
              when /^#{@cluster}-fdb-(\d+)$/ then fdb_name_index = $1.to_i
              when /^#{@cluster}-sql-(\d+)$/ then sql_name_index = $1.to_i
              end
            end
            @new_cluster = false
          end
        end
            
        ndisks = config[:fdb_processes].to_i
        num_fdb.times do |i|
          node = {
            :name => "#{@cluster}-fdb-#{fdb_name_index+=1}",
            :description => "#{@cluster} FDB Server #{i+1}",
            :vm_template => config[:fdb_vm_template],
            :vdisk_template => config[:vdisk_template],
            :data_disks => ndisks,
            :iptables => ['ssh','fdb'],
            :fdb => { 
              'cluster' => @cluster,
              'disks' => [],
              'server' => []
            },
            :run_list => [
              'recipe[fdb-vzcloud::fdb_mounts]',
              'recipe[fdb-vzcloud::iptables]',
              'recipe[fdb::server]'
            ]
          }
          ndisks.times do |j|
            node[:fdb]['disks'] << "xvd#{(97+j+1).chr()}"
            node[:fdb]['server'] << {
              'id' => 4500 + j,
              'datadir' => "/data#{j}/fdb/$ID",
              'coordinator' => j == 0 && i == 0 && @new_cluster
            }
          end
          @nodes << node
        end

        num_sql.times do |i|
          node = {
            :name => "#{@cluster}-sql-#{sql_name_index+=1}",
            :description => "#{@cluster} SQL Layer #{i+1}",
            :vm_template => config[:sql_vm_template],
            :vdisk_template => config[:vdisk_template],
            :iptables => ['ssh','sql'],
            :fdb => { 
              'cluster' => @cluster
            },
            :run_list => [
              'recipe[fdb-vzcloud::iptables]',
              'recipe[fdb::sql_layer]'
            ]
          }
          @nodes << node
        end
        
        @nodes.each do |node|
          node[:ipaddress] ||= @next_ip_address.tap do |addr|
                                 @next_ip_address = IPAddr.new(IPAddr.new(addr).to_i + 1, Socket::AF_INET).to_s
                               end
        end

        list = ['Name', 'IP', 'VM', 'OS', 'Coordinator'].collect {|h| ui.color(h, :bold)}
        @nodes.each do |node|
          unless node[:node]
            list.concat [
              node[:name],
              node[:ipaddress],
              node[:vm_template],
              node[:vdisk_template],
              node[:fdb]['server'] && node[:fdb]['server'][0]['coordinator'] ? '*' : ''
            ]
          end
        end
        puts ui.list(list, :columns_across, 5)
        ui.confirm @new_cluster ? 
          "Create cluster #{@cluster} with above nodes" :
          "Add above nodes to #{@cluster}"

        @all_nodes = @nodes
        @nodes = @all_nodes.select {|node| node[:node].nil? }

        if @new_cluster
          fcc = FdbClusterCreate.new
          fcc.merge_configs
          fcc.name_args = [ @cluster ]
          fcc.run
        end

        @vm_create.config[:network] = 'lan'
        @vm_create.config[:network_bandwidth] = 500
        @vm_create.config[:data_disk_iops] = 5000
        @nodes.each do |node|
          ui.msg "Creating VM #{node[:name]}"
          @vm_create.name_args = [ node[:name], node[:vm_template], node[:vdisk_template] ]
          @vm_create.config[:description] = node[:description]
          @vm_create.config[:data_disks] = node[:data_disks] || 0
          node[:job] = @vm_create.run
          node[:href] = node[:job]['target']['href']
        end

        vz_rest = @vm_create.vz_rest
        vz_rest.start do |http|
          @nodes.each do |node|
            ui.msg "Waiting for #{node[:name]} to start"
            vm = wait_for_power_state node, :on, vz_rest, http
            node[:password] = vm['id'] =~ /-(\w+)$/ && $1
            vnics = vz_rest.get vm['vnics']['href']
            node[:mac] = vnics['items'][0]['mac']

            ui.msg "Powering off #{node[:name]}"
            node[:job] = vz_rest.post node[:controllers]['powerOff']['href'], nil, http
          end
          @nodes.each do |node|
            wait_for_power_state node, :off, vz_rest, http
          end
        end

        pfsense = @dhcp_create.pfsense
        pfsense.session do |session, http|
          @nodes.each do |node|
            ui.msg "Adding mapping for #{node[:name]} from #{node[:mac]} to #{node[:ipaddress]}"
            pfsense.service_dhcp_static_add session, http,
              'mac' => node[:mac], 'ipaddr' => node[:ipaddress],
              'hostname' => node[:name], 'descr' => node[:description]
          end
          pfsense.service_dhcp_apply session, http
        end

        vz_rest.start do |http|
          @nodes.each do |node|
            ui.msg "Powering on #{node[:name]}"
            node[:job] = vz_rest.post node[:controllers]['powerOn']['href'], nil, http
          end
          @nodes.each do |node|
            ui.msg "Waiting for #{node[:name]} to start"
            wait_for_power_state node, :on, vz_rest, http
          end
        end

        ui.confirm "Are machines running (and not stuck in Grub)"

        ui.msg "Once only setup"
        cmd = 'wget -nv -O - https://gist.github.com/MMcM/8b74464bd87445f7c0f5/raw/e3ab68ee51a9be58ee7fd0bc4facf6e2a80a1145/vzcloud.sh | bash'

        begin
          Net::SSH::Multi.start do |session|
            ssh_config = Chef::Config[:knife]
            if gw_host = ssh_config[:ssh_gateway]
              gw_host, gw_user = ssh_config[:ssh_gateway].split('@').reverse
              gw_host, gw_port = gw_host.split(':')
              gw_user ||= ssh_config[:ssh_user]
              gw_port ||= 22
              session.via gw_host, gw_user, :port => gw_port,
                :keys => File.expand_path(ssh_config[:ssh_identity_file]),
                :keys_only => true
            end

            @nodes.each do |node|
              session.use node[:ipaddress], :user => 'vzcloud', :password => node[:password]
            end

            session.exec cmd

            session.loop
          end
        rescue Exception => ex
          ui.error "#{ex.class.name}: #{ex.message}"
        end

        ui.confirm "Are machines booted and know their names"

        @nodes.each do |node|
          bs = Chef::Knife::Bootstrap.new
          bs.merge_configs
          bs.name_args = [ node[:name] ]
          bs.config[:chef_node_name] = node[:name]
          bs.config[:ssh_user] = 'vzcloud'
          bs.config[:use_sudo] = true
          bs.config[:identity_file] = Chef::Config[:knife][:ssh_identity_file]
          bs.run

          chef_node = Chef::Node.load(node[:name])
          chef_node.normal['iptables'] = node[:iptables]
          chef_node.normal['fdb'] = node[:fdb]
          node[:run_list].each {|rli| chef_node.run_list << rli }
          chef_node.save
        end

        if config[:run_chef_client]
          ui.msg "Waiting for index to update"
          node_query = Chef::Search::Query.new
          query = "fdb_cluster:#{@cluster}"
          begin
            sleep 5
            nodes, start, total = node_query.search(:node, query)
          end until total >= @all_nodes.size

          ui.msg "Running chef on nodes"
          ssh = Chef::Knife::Ssh.new
          ssh.merge_configs
          ssh.name_args = [ query, "sudo chef-client" ]
          ssh.run
        end
      end

      def wait_for_power_state (node, to_state, vz_rest, http)
        begin
          if node[:job]
            begin
              vz_rest.wait_for_job node[:job]['href'], http
            rescue Exception => ex
              ui.warn "#{node[:name]}: #{ex.class.name}: #{ex.message}"
            end
            node[:job] = nil
          end
          vm = vz_rest.get node[:href]
          node[:controllers] = vm['controllers']
          state_now = if node[:controllers]['powerOff'] then :on
                      elsif node[:controllers]['powerOn'] then :off
                      else raise "#{node[:name]} has no power controller" end
          return vm if state_now == to_state
          ui.msg "Trying to power #{to_state} #{node[:name]} again"
          node[:job] = vz_rest.post node[:controllers][to_state == :on ? 'powerOn' : 'powerOff']['href'], nil, http
        end while true
      end
    end
  end
end
