
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'pfsense_base'

class Chef
  class Knife
    class PfsenseDhcpStaticList < Knife

      include Knife::PfsenseBase

      banner "knife pfsense dhcp static list"

      def run
        list = ['MAC', 'IP', 'Name', 'Description'].collect {|h| ui.color(h, :bold)}
        pfsense.session do |session, http|
          pfsense.service_dhcp_static(session, http).each do |row|
            row.shift
            list.concat row
          end
        end
        puts ui.list(list, :columns_across, 4)
      end

    end
  end
end
