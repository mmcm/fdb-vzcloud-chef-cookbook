
require 'chef/knife/ssh'

class Chef
  class Knife
    class Ssh
      def configure_gateway
        config[:ssh_gateway] ||= Chef::Config[:knife][:ssh_gateway]
        if config[:ssh_gateway]
          gw_host, gw_user = config[:ssh_gateway].split('@').reverse
          gw_host, gw_port = gw_host.split(':')
          gw_opts = gw_port ? { :port => gw_port } : {}

          # ADDED NEXT TWO LINES
          gw_opts[:keys] = File.expand_path(config[:identity_file]) if config[:identity_file]
          gw_opts[:keys_only] = true if config[:identity_file]

          session.via(gw_host, gw_user || config[:ssh_user], gw_opts)
        end
      rescue Net::SSH::AuthenticationFailed
        user = gw_user || config[:ssh_user]
        prompt = "Enter the password for #{user}@#{gw_host}: "
        gw_opts.merge!(:password => prompt_for_password(prompt))
        session.via(gw_host, user, gw_opts)
      end
    end
  end
end
