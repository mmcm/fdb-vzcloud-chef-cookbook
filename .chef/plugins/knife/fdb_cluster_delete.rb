
class Chef
  class Knife
    class FdbClusterDelete < Knife

      banner "knife fdb cluster delete NAME"

      deps do
        require 'chef/data_bag_item'
        require 'chef/search/query'
      end

      def run
        @cluster_name = @name_args[0]
        if @cluster_name.nil?
          show_usage
          ui.fatal "You must specify a cluster name"
          exit 1
        end

        cluster_item = Chef::DataBagItem.load('fdb_cluster', @cluster_name)

        confirm "Do you really want to delete #{@cluster_name}"

        cluster_item.destroy 'fdb_cluster', @cluster_name
      end
    end
  end
end
