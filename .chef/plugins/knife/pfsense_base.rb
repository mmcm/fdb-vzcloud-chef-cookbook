
class Chef
  class Knife
    module PfsenseBase

      def self.included(base)
        base.class_eval do

          deps do
            require 'pfsense/scraper'
          end

          option :user,
            :short => '-u',
            :long => '--user USER',
            :description => 'Admin user name',
            :proc => Proc.new {|val| Chef::Config[:knife][:pfsense_user] = val}

          option :password,
            :short => '-p',
            :long => '--password PASSWORD',
            :description => 'Admin user password',
            :proc => Proc.new {|val| Chef::Config[:knife][:pfsense_password] = val}

          option :host,
            :short => '-h',
            :long => '--host HOST',
            :description => 'Cloud API host',
            :default => 'pfsense.fdb.private'

          option :port,
            :short => '-p',
            :long => '--port PORT',
            :description => 'Cloud API port',
            :default => 443

        end
      end

      def pfsense
        @pfsense ||= begin
           user = Chef::Config[:knife][:pfsense_user]
           password = Chef::Config[:knife][:pfsense_password]

           unless user && password
             show_usage
             ui.fatal "You must specify access credentials here or in knife.rb"
             exit 1
           end

           Pfsense::Scraper.new(config[:host], config[:port], user, password)
        end
      end
    end
  end
end
