
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonVmList < Knife

      include Knife::VerizonBase

      banner "knife verizon vm list"

      def run
        vms = nil
        vz_rest.start do |http|
          vms = vz_rest.get "#{API_ROOT}/vm/?limit=200", http
        end
        vms['items'].sort! {|v1,v2| v1['name'] <=> v2['name']}
        list = ['Name', 'Status', 'Description'].collect {|h| ui.color(h, :bold)}
        vms['items'].each do |vm|
          ['name', 'status', 'description'].each do |key|
            list << vm[key]
          end
        end
        puts ui.list(list, :columns_across, 3)
      end
    end
  end
end
