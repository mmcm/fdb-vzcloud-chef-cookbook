
class Chef
  class Knife
    class FdbClusterCreate < Knife

      banner "knife fdb cluster create NAME (options)"

      option :redundancy,
        :short => '-r',
        :long => '--redundancy MODE',
        :description => 'Redundancy mode',
        :default => 'single'

      option :storage,
        :short => '-s',
        :long => '--storage ENGINE',
        :description => 'Storage engine',
        :default => 'ssd'

      deps do
        require 'chef/data_bag'
        require 'chef/data_bag_item'
      end

      def run
        @cluster_name = @name_args[0]
        if @cluster_name.nil?
          show_usage
          ui.fatal "You must specify a cluster name"
          exit 1
        end

        if data_bag = Chef::DataBag.load('fdb_cluster')
          if data_bag[@cluster_name]
            ui.fatal "There is already a cluster named #{ui.color(@cluster_name, :bold)}"
            exit 1
          end
        end

        cluster = {
          'id' => @cluster_name,
          'redundancy' => validate(config[:redundancy], ['single', 'double', 'triple']),
          'storage' => validate(config[:storage], ['memory', 'ssd']),
          'unique_id' => unique_id
        }
        cluster_item = Chef::DataBagItem.new
        cluster_item.data_bag 'fdb_cluster'
        cluster_item.raw_data = cluster
        cluster_item.save
        ui.msg "Cluster #{@cluster_name} created"
      end

      def validate(option, allowed)
        unless allowed.include?(option)
          ui.fatal "Invalid option: #{option}"
          exit 1
        end
        option
      end

      def unique_id
        chars = [('0'..'9'), ('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
        (1..8).map { chars[rand(chars.length)] }.join
      end
    end
  end
end
