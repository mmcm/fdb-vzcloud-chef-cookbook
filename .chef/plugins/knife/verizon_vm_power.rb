
current_dir = File.dirname(__FILE__)
$:.unshift current_dir unless $:.include?(current_dir)
require 'verizon_base'

class Chef
  class Knife
    class VerizonVmPower < Knife

      include Knife::VerizonBase

      banner "knife verizon vm power on/off NAME"

      def run
        if @name_args.length < 2
          show_usage
          ui.fatal "You must specify a machine name"
          exit 1
        end
        verb, @vm_name = @name_args
        case verb
        when 'on','off'
        else
          ui.fatal "Must power on or off"
          exit 1
        end

        vz_rest.start do |http|
          vms = vz_rest.get "#{API_ROOT}/vm/?limit=200", http
          vm = vms['items'].detect {|vm| vm['name'] == @vm_name}
          if vm
            # reset & reboot seem to never be here, not even in the UI.
            controller = vm['controllers']["power#{verb.capitalize}"]
            if controller
              vm = vz_rest.post controller['href'], nil, http
            else
              ui.fatal "Virtual machine is already powered #{verb}"
              exit 1
            end
          else
            ui.fatal "Virtual machine not found"
            exit 1
          end
        end
      end
    end
  end
end
