# Chef cookbook for FoundationDB on Verizon Cloud

## Recipes

* ```fdb-vzcloud::fdb_mounts``` mount data disks for FDB Server

* ```fdb-vzcloud::iptables``` flexible iptables setup for FDB or SQL

## Knife plugins

* ```fdb cluster create``` create new cluster

* ```fdb cluster delete``` delete cluster (but not machines)

* ```fdb cluster list``` list clusters

* ```fdb cluster show``` show cluster and machines

* ```fdb verizon cluster create``` create new / add to existing cluster (with machines)

* ```fdb verizon cluster delete``` delete cluster (and machines)

* ```pfsense dhcp static create``` add static mapping

* ```pfsense dhcp static delete``` delete static mapping

* ```pfsense dhcp static list``` list static mappings

* ```verizon disk template list``` list virtual disk templates

* ```verizon net list``` list virtual networks

* ```verizon vm create``` create virtual machine

* ```verizon vm delete``` delete virtual machine and optionally disks

* ```verizon vm list``` list virtual machines

* ```verizon vm power``` power on/off virtual machine

* ```verizon vm show``` show details of virtual machine

* ```verizon vm template list``` list virtual machine templates
