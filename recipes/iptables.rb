#
# Cookbook Name:: fdb-vzcloud
# Recipe:: iptables
#

# Replace the simpler rules that come with vzcloud.

file "/etc/network/if-pre-up.d/iptablesload" do
  action :delete
  only_if { ::File.exists?("/etc/network/if-pre-up.d/iptablesload") }
end

file "/etc/iptables.rules" do
  action :delete
  only_if { ::File.exists?("/etc/iptables.rules") }
end

include_recipe 'iptables'

node['iptables'].each_with_index do |rule, i|
  if i < node['iptables'].size - 1
    iptables_rule rule do
      source "iptables_#{rule}.erb"
    end
  else
    template "/etc/iptables.d/#{rule}" do
      source "iptables_#{rule}.erb"
      mode 0644
      backup false
      notifies :run, "execute[rebuild-iptables]", :immediately
      action :create
    end
  end
end
