#
# Cookbook Name:: fdb-vzcloud
# Recipe:: fdb_mounts
#

include_recipe 'fdb::user'

node['fdb']['disks'].each_with_index do |dev, idx|
  directory "/data#{idx}" do
    mode "0755"
  end

  execute "mkfs #{dev}" do
    command "mkfs -t ext4 /dev/#{dev}"
    not_if "grep -qs /dev/#{dev} /proc/mounts"
  end

  mount "/data#{idx}" do
    device "/dev/#{dev}"
    fstype 'ext4'
    options 'noatime'           # ,discard
    action [:enable,:mount]
  end

  directory "/data#{idx}/fdb" do
    owner "foundationdb"
    group "foundationdb"
    mode "0700"
  end
end
