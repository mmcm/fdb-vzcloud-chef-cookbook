name             'fdb-vzcloud'
maintainer       'Mike McMahon'
maintainer_email 'mike.mcmahon@foundationdb.com'
license          'All rights reserved'
description      'Verizon Cloud Configuration'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.5'

depends           "fdb"
depends           "iptables"
